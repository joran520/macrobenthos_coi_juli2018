#!/bin/bash
# (c) Annelies Haegeman 2017
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to remove primers from a set of sequencing files in a specific folder
# The forward and reverse file should end in _R1_001.fastq.gz and _R2_001.fastq.gz respectively.

#Specify here the folder where your samples are:
INPUT_FOLDER=/home/genomics/sderycke/Macrobenthos_COI_Juli2018/01_Raw_data/PrimersetB/
#Specify here the folder where you want your output files to be written to:
OUTPUT_FOLDER=/home/genomics/sderycke/Macrobenthos_COI_Juli2018/02_Trimmomatic/PrimersetB/

date
echo

#change directory to specified folder
cd $INPUT_FOLDER

# create several new directories to put the output after each step
mkdir  -p $OUTPUT_FOLDER

# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
FILES=( *1_001.fastq.gz )

#Loop over all files and do all the commands
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed (-1.fastq.gz)
SAMPLE=`basename $f 1_001.fastq.gz`

echo
echo "PROCESSING sample $SAMPLE "
echo

#PRIMER REMOVAL
echo "removing primers for $SAMPLE...."
#remove forward primer using Trimmomatic. 
java -jar /usr/local/bioinf/Trimmomatic-0.32/trimmomatic-0.32.jar SE "$SAMPLE"1_001.fastq.gz $OUTPUT_FOLDER/"$SAMPLE"1.fq.gz HEADCROP:20
#remove reverse primer using Trimmomatic
java -jar /usr/local/bioinf/Trimmomatic-0.32/trimmomatic-0.32.jar SE "$SAMPLE"2_001.fastq.gz $OUTPUT_FOLDER/"$SAMPLE"2.fq.gz HEADCROP:26

echo "done removing primers for $SAMPLE."

echo
date

done
