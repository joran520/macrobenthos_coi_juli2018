﻿###########################################################
##   Analyse pilootproject GEANS Thornthonbank 2019      ##
##                 Laure Van den Bulcke                  ##
###########################################################


#!/bin/bash
# (c) Annelies Haegeman 2017
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to remove primers from a set of sequencing files in a specific folder
# The forward and reverse file should end in _R1_001.fastq.gz and _R2_001.fastq.gz respectively.

#Specify here the folder where your samples are:
#raw data in map "all_raw_seq_data" BUT copy it first in your 
INPUT_FOLDER=/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/01_Raw_data/PrimersetE/
#Specify here the folder where you want your output files to be written to:
OUTPUT_FOLDER=/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/02_Trimmomatic/PrimersetE/

date #geeft datum van vandaag
echo #zoiets als print

#change directory to specified folder
cd $INPUT_FOLDER

# create several new directories to put the output after each step
mkdir -p $OUTPUT_FOLDER

# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
# forward files end in _R1_001.fastq.gz, reverse files end in _R2_001.fastq.gz
FILES=(*1_001.fastq.gz)
echo "$FILES" #controle:toon variable

#Loop over all files and do all the commands
#Using Trimmomatic: A flexible read trimming tool for Illumina NGS data, headcrop: Removes the specified number of bases, regardless of quality, from the beginning of the read.


for f in "${FILES[@]}" #START LOOP
do 
#Define the variable SAMPLE who contains the basename where the extension is removed (-1.fq.gz) => get basename (+ 1=forward, + 2=reverse)
SAMPLE=`basename $f 1_001.fastq.gz`

echo
echo "PROCESSING sample $SAMPLE "
echo

#PRIMER REMOVAL
#SE = single end
#primerset: mlCOIintF  / LoboR
# forward primer: mlCOIintF: GGWACWGGWTGAACWGTWTAYCCYCC (26 = length of the forward primer)
#Correcte Lobo R primer in 5’-3’:TAAACYTCWGGRTGWCCRAARAAYCA (26 = length of the reverse primer)

echo "removing primers for $SAMPLE...."
#remove forward primer using Trimmomatic. 

TrimmomaticSE "$SAMPLE"1_001.fastq.gz $OUTPUT_FOLDER/"$SAMPLE"1.fq.gz HEADCROP:26
#remove reverse primer using Trimmomatic
TrimmomaticSE "$SAMPLE"2_001.fastq.gz $OUTPUT_FOLDER/"$SAMPLE"2.fq.gz HEADCROP:26

echo "done removing primers for $SAMPLE."

echo
date

done # END LOOP

