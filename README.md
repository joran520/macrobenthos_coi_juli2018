# Macrobenthos_COI_Juli2018

## Introduction
This pipeline is a custom pipeline built to analyse amplicon sequencing data resulting from a library preparation method described in (file?).

Library preparation
The library preparation method consists of a PCR step with custom primers to amplify the region of interest. In this case 5 different primer sets were used named:.



Primerset
Forward primer
Reverse primer
Reference




Primerset A
Forward primer
Reverse primer
name, Date


Primerset B
Forward primer
Reverse primer
name, Date

Primerset C
Forward primer
Reverse primer
name, Date

Primerset D
Forward primer
Reverse primer
name, Date

Primerset E
Forward primer
Reverse primer
name, Date



Explenation of how en why the method was used.

The disadvantages of this library prep method are:

XXX


### Data analysis pipeline
The data analysis pipeline consists of three main steps:

Demultiplexing
Cleanup (quality filtering, adapter removal,primer removal)
Visualizing macrobenthos communities
Comparing the different primer sets to each other


The pipeline uses primarily third party software (described below) called through shell scripts.

## Installation requirements
The following software should be installed on your system to run the scripts in this repository (older or newer versions of the software might work as well, but this was not tested). Make sure to check the license of each program to verify whether you are allowed to use it or not.

Operating system: Ubuntu 20.04.01

Trimmomatic (version) (Trimming tool, primer removal, should be in $PATH)

Blastx (version)

OBITools 1.2.13 (ecopcr, commands should be in $PATH)



## Pipeline
The analysis pipeline consists of a multiple shell/R scripts script which does the following:

Removal of primers using Trimmomatic

Quality control and filtering of the reads, merging of forward and reverse reads, removing chimera's, (rarefaction),taxonomy asigning using DADA2 pipeline in R/Rstudio.

Blastx was used to identify non matched sequences from the RDP identifier in DADA2.

The communities were then represented in graphs using R/Rstudio.

In following script the different communities were compared to each other to determine the best primerset that represents most of the sequences/species.

edited by joran


